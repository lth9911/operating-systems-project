from semaphores import Semaphore
from sys import argv
import random
from time import sleep

def main():
	n = int(argv[1])
	sleep(random.randrange(5))
	for i in range (n):
		semaphore = Semaphore(1)
		semaphore.wait()
		sleep_time = random.randrange(5)
		thread_id = random.randrange(0,2)
		print("Thread", thread_id, "got the lock.")
		print("Thread", thread_id, "holding the lock for", sleep_time, "seconds.")
		
		semaphore.signal()
		print ("Thread", thread_id, "got the lock.")
		
		semaphore.wait()
		sleep_time = random.randrange(5)
		thread_id = random.randrange(0,1)
		print("Thread", thread_id, "got the lock.")
		print("Thread", thread_id, "holding the lock for", sleep_time, "seconds.")
		
		semaphore.signal()
		print ("Thread", thread_id, "released the lock.")
		print ("Lock released.")

	print ("Main thread exiting.")

main()
