# Implement Semaphores

import _thread

class Semaphore:
	def __init__(self, value):
		self.count = value
		self.queue = []
		self.mutex = _thread.allocate_lock()
	def wait(self):
		self.mutex.acquire()
		self.count = self.count - 1
		if self.count < 0:
			wlock = _thread.allocate_lock()
			wlock.acquire()
			self.queue.append(wlock)
			self.mutex.release()
			wlock.acquire()
		else:
			self.mutex.release()
	def signal(self):
		self.mutex.acquire()
		self.count = self.count + 1
		if self.count <= 0:
			wlock = self.queue[0]
			del self.queue[0]
			wlock.release()
		self.mutex.release()
