#!/usr/local/bin/python
# Dining Philosophers shell
# Code shamefully borrowed from Dr. John Zelle. Thanks, Dr. Zelle!
# Hieu & Tan 

# system modules
from random import randint
import _thread, time, signal

# local modules
from DPGui import DPDisplay
from semaphores import Semaphore

NUM_PHILOSOPHERS = 5   # changeable from command-line
THINKMAX = 6
EATMAX = 2

def philosopher(i, display, forks):
    # behavior of the ith philosopher
    p1,p2=i,(i+1)%NUM_PHILOSOPHERS
    display.setPhil(i,"thinking")
    while 1:
        time.sleep(randint(0,THINKMAX))
        display.setPhil(i,"hungry")
        forks[p1].wait()
        display.setFork(p1,"inuse")
        time.sleep(1)
        forks[p2].wait()
        display.setFork(p2,"inuse")
        display.setPhil(i,"eating")
        time.sleep(randint(1,EATMAX))
        display.setPhil(i,"thinking")
        display.setFork(p2,"free")
        forks[p2].signal()
        display.setFork(p1,"free")
        forks[p1].signal()
    
        
def main():
    d = DPDisplay(NUM_PHILOSOPHERS)
    forks = []
    for i in range(NUM_PHILOSOPHERS):
        forks.append(Semaphore(1))
        d.setFork(i,"free")
    for i in range(NUM_PHILOSOPHERS):
        _thread.start_new_thread(philosopher, (i,d,forks))
    d.pause()  # wait for mouse click to keep main thread alive


# Use command-line argument to change number of philosophers
from sys import argv
if len(argv) > 1:
    NUM_PHILOSOPHERS = int(argv[1])

main()         # Run main program to launch philosophers
