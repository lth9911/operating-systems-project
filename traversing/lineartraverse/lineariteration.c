#include <linux/sched.h>
int functn_init(void) { 
	struct task_struct *task;
	for_each_process(task) {
		/* on each iteration task points to the next task */
		printk("Task: %s, Pid[%d], State: %d\n",
			task->comm,
			task->pid,
			task->state
		);
	}
	return 0;
}

int functn_exit(void) {
	printk("Exiting");
}

/* Macros for registering module entry and exit points. */
module_init( functn_init );
module_exit( functn_exit );
