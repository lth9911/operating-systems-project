#include <linux/sched.h>
int functn_init (void) {
	struct task_struct *task;
	struct list_head *list;
	list_for_each(list, &init_task->sibling) {
		task = list_entry(list, struct task_struct, sibling);
		/* task points to the next child in list */
		/*printk("%d\n", task->pid);*/
	}
	return 0;	
}

int functn_exit(void) {
	printk("Exiting");
}

/* Macros for registering module entry and exit points. */
module_init( functn_init );
module_exit( functn_exit );
