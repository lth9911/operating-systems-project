#!/usr/bin/env python
# lullaby.py
# Hieu Le, Tan Nguyen

import os, time, sys

def main():
	parent_id = os.getpid()
	pid = os.fork()
	if pid == 0:
		print "In Child Process"
		print "Parent ID is: ", parent_id
		time.sleep(10)
		print "Terminating"
		sys.exit(13)
	else:
		print "In Parent Process"
		print "Child ID is: ", pid
		pid, status = os.wait()
		print "Status: ", status/256
 
main()
