# shell-shell.py
# Hieu Le, Tan Nguyen

import os, sys

def execute(args):
	pid = os.fork()
	if pid == 0: # In child process
		try:
			os.execvp(args[0],args)
		except:
			print "Command Not Found"
	else: # In parent process
		if args[-1] != "&":
			os.wait()
		if args[-1] == "&":
			print "Process Number", pid

def main():
	while True: # An infinite loop
		commandLine = raw_input("your prompt% ")
		if commandLine == "exit":
	        	break  # Quit shell on exit
		if commandLine == "":
	            	continue # Reprint prompt on blank line.
		if commandLine.strip() != "":
	            # split at spaces to create list of strings.
	            	args = commandLine.split()
	            # pass the list of strings to the execute command.
	            	execute(args)
			

main()
