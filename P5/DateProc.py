#!/usr/bin/env python
# DateProc.py
# Hieu Le, Tan Nguyen

import os, sys

def main():
	pid = os.fork()
	if pid == 0:
		os.execl("/bin/date", "date")
	else:
		os.wait()
main()
