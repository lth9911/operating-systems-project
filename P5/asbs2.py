#!/usr/bin/env python
# asbs2.py
# Hieu Le, Tan Nguyen

import os, sys

HOWMANY = 5000

def child(pid):
	if pid == 0:
		for i in range (HOWMANY):
			sys.stdout.write("a")
			sys.stdout.flush()
	else:
		for i in range (HOWMANY):
			sys.stdout.write("B")
			sys.stdout.flush()

def main():
	pid = os.fork()
	if pid == 0:
		new_pid = os.fork()
		child(new_pid)
	else:
		os.wait()
		print "All Done"
		sys.exit(0)
main()	
